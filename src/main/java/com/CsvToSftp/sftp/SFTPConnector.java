package com.CsvToSftp.sftp;

import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

 
/**
 * Clase encargada de establecer conexion y ejecutar comandos SFTP.
 */
public class SFTPConnector {
 
	static final Logger logger = Logger.getLogger(SFTPConnector.class);
    /**
     * Sesion SFTP establecida.
     */
    private Session session;
    
    public SFTPConnector(){
    	URL url = Loader.getResource("src/main/resources/files/log4j.properties");
		PropertyConfigurator.configure(url);
    }
 
    /**
     * Establece una conexion SFTP.
     *
     * @param username Nombre de usuario.
     * @param privateKey  PATH del archivo de contrasena.
     * @param host     Host a conectar.
     * @param port     Puerto del Host.
     *
     * @throws JSchException          Cualquier error al establecer
     *                                conexión SFTP.
     * @throws IllegalAccessException Indica que ya existe una conexion
     *                                SFTP establecida.
     */
    public void connect(String username, String privateKey, String host, int port)
        throws JSchException, IllegalAccessException {
        if (this.session == null || !this.session.isConnected()) {
            JSch jsch = new JSch();
            jsch.addIdentity(privateKey);
            this.session = jsch.getSession(username, host, port); 
            this.session.setConfig("StrictHostKeyChecking", "no");
            this.session.connect();
        } else {
            throw new IllegalAccessException("Sesión SFTP ya iniciada.");
        }
    }
    
  
    /**
     * Añade un archivo al directorio FTP usando el protocolo SFTP.
     *
     * @param ftpPath  Path del FTP donde se agregará el archivo.
     * @param filePath Directorio donde se encuentra el archivo a subir en
     *                 disco.
     * @param fileName Nombre que tendra el archivo en el destino.
     * @throws IllegalAccessException 
     * @throws SftpException 
     * @throws JSchException 
     * @throws Exception 
     */
    public final void addFile(String ftpPath,  String[] pathFileNames,  String[] filesName) throws RuntimeException, IllegalAccessException, SftpException, JSchException {
        if (this.session != null && this.session.isConnected()) {
 
            ChannelSftp channelSftp = (ChannelSftp) this.session.openChannel("sftp");
            channelSftp.connect();
            channelSftp.cd(ftpPath); 
            boolean error = false;
            try{
	            for(int i = 0; i < filesName.length; i++){
	            	logger.info(String.format("Creando archivo %s en el directorio %s", filesName[i], ftpPath));
	            	channelSftp.put(pathFileNames[i], filesName[i]);            	
	            }	 
	            logger.info("Archivos subidos exitósamente!");
            }
        	catch(Exception e){
        		logger.error("Error durante eliminación y subida de archivos al SFTP", e);
        		error = true;
        	}
            finally{
	            channelSftp.exit();
	            channelSftp.disconnect();
            }
            if(error)
            	throw new RuntimeException("Ejecución SFTP Fallida.");
        } else {
            throw new IllegalAccessException("No existe sesión SFTP iniciada.");
        }
    }  
    /**
     * Cierra la sesion SFTP.
     */
    public final void disconnect() {
        this.session.disconnect();
    }
}