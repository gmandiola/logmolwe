package com.CsvToSftp.dao;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;


public class ConnectionProvider {

	static final Logger logger = Logger.getLogger(ConnectionProvider.class);
	
	private ConnectionProvider() {
		URL url = Loader.getResource("src/main/resources/files/log4j.properties");
		PropertyConfigurator.configure(url);
	}
	
	public static Connection getConnection() {

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {			
			logger.error("org.postgresql.Driver no encontrado.", e);
		}
		
		Connection connection = null;

		try {
			
			Properties p = new Properties();
			p.load(new FileReader("config/config.properties"));	
			
			connection = DriverManager.getConnection(p.getProperty("conexion"), p.getProperty("user"),p.getProperty("pass"));

		} catch (SQLException | IOException e) {
			logger.error("Error de conexión a la Base de Datos.", e);
		}

		return connection;

	}
}