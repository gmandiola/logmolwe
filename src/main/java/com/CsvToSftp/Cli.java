package com.CsvToSftp;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Cli {
	 private static final Logger logger = Logger.getLogger(Cli.class.getName());	 
	 private String[] args = null;
	 private Options options = new Options();

	 public Cli(String[] args) {

	  this.args = args;
	  options.addOption("f", "fecha-ejecucion", true, "Fecha de ejecución de la tarea.");
	 }

	 public CommandLine parse() throws RuntimeException, FileNotFoundException, IOException {
		 
		 URL url = Loader.getResource("src/main/resources/files/log4j.properties");
		 PropertyConfigurator.configure(url);
		 CommandLineParser parser = new DefaultParser();
		 CommandLine cmd = null;
		 try {
			 
		   cmd = parser.parse(options, args);
		   
		   
		   if (cmd.hasOption("f")) {
			   	logger.info("Cli - Validando argumento --fecha-ejecucion = " + cmd.getOptionValue("f"));
			   	String regexp = "\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])*";	
			    boolean valid = Pattern.matches(regexp, cmd.getOptionValue("f"));
			    if(!valid)
			    	throw new RuntimeException("Formato no valido para --fecha-ejecucion = " + cmd.getOptionValue("f"));
		   } else {
			   logger.info("Es obligatorio el argumento --fecha-ejecucion");
			   help();
		   }
		   
		  } catch (ParseException e) {
			  logger.error("No se pudo efectuar el parse de los argumento enviados.", e);
			  help();
		  }
	  
		 return cmd;
	 }

	 private void help() {
		  HelpFormatter formater = new HelpFormatter();
		  formater.printHelp("Main", options);
		  System.exit(0);
		 }
	}