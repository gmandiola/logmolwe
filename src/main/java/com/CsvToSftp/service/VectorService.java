package com.CsvToSftp.service;

import java.sql.SQLException;

public interface VectorService {

	 public Object getVector() throws SQLException;
	 
}
