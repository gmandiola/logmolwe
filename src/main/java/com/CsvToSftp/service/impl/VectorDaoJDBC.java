package com.CsvToSftp.service.impl;


import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;

import com.CsvToSftp.dao.ConnectionProvider;
import com.CsvToSftp.service.VectorDao;



public class VectorDaoJDBC implements VectorDao{

	static final Logger logger = Logger.getLogger(VectorDaoJDBC.class);	
	
	private static String queryextraccion =
			"	select nexus.id as id_log,"+
					"	nexus.codigo_servicio_nexus as funcionalidad, 			"+
					"	       case  when nexus.codigo_resultado is null then 'NULL' "+
					"           when nexus.codigo_resultado = '0' then 'OK'		"+
					"	             else nexus.mensaje_resultado				"+
					"	       end as resultado,								"+
					"      TO_CHAR( nexus.fecha_hora_inicio_trx,'YYYY-MM-DD HH24.MI.SS.ssss0') as tiempo_inicial, 	"+
					"TO_CHAR( nexus.fecha_hora_fin_trx,'YYYY-MM-DD HH24.MI.SS.ssss0') as tiempo_fin, 			"+
					"	       nexus.tiempo_ejecucion as tiempo_ejec,			"+
					"	       tarjeta.numero_tarjeta as info_adicional,		"+
					"	       	organizacion.codigo,							"+
					"	       canal.descripcion								"+
					"from admmrie.mrie_consulta_nexus as nexus					"+ 
					"inner join admmcta.mcta_maestro_tarjeta as tarjeta " + 
					"    on nexus.id_tarjeta = tarjeta.id_tarjeta " + 
					"inner join admmcor.mcor_canal as canal " + 
					"    on nexus.id_canal = canal.id " + 
					"inner join admmcta.mcta_marca_tarjeta as marca " + 
					"    on tarjeta.id_marca_tarjeta = marca.id " + 
					"inner join admmref.mref_organizacion as organizacion " + 
					"    on marca.id_organizacion = organizacion.id " + 
					"inner join admmref.mref_categoria_tar_cta as categoria " + 
					"    on marca.id_categoria_tar_cta = categoria.id       "+
					"	where nexus.fecha_creacion=? " +
					"   UNION " + 
					" select nexus.id, nexus.codigo_servicio_nexus, " + 
					"      case  when nexus.codigo_resultado is null then 'NULL' " + 
					"             when nexus.codigo_resultado = '0' then 'OK' " + 
					"             else nexus.mensaje_resultado" + 
					"       end as resultado," + 
					"      TO_CHAR( nexus.fecha_hora_inicio_trx,'YYYY-MM-DD HH24.MI.SS.ssss0') as tiempo_inicial, " + 
					"TO_CHAR( nexus.fecha_hora_fin_trx,'YYYY-MM-DD HH24.MI.SS.ssss0') as tiempo_fin, " + 
					"       nexus.tiempo_ejecucion, " + 
					"       cuenta.numero_cuenta as info_adicional," + 
					"       organizacion.codigo," + 
					"       canal.descripcion "+
					"	from admmrie.mrie_consulta_nexus as nexus " +
					"	inner join admmcta.mcta_maestro_cuenta as cuenta " + 
					"    	on nexus.id_cuenta = cuenta.id_cuenta " +  
					"	inner join admmcta.mcta_marca_tarjeta as marca " + 
					"    	on cuenta.id_marca_tarjetas = marca.id " + 
					"	inner join admmref.mref_organizacion as organizacion " + 
					"    	on marca.id_organizacion = organizacion.id " + 
					"	inner join admmref.mref_categoria_tar_cta as categoria " + 
					"    	on marca.id_categoria_tar_cta = categoria.id" + 
					"	inner join admmcor.mcor_canal as canal " + 
					"    	on nexus.id_canal = canal.id " +
					"	where nexus.fecha_creacion= ?  " ;
	
	public VectorDaoJDBC() {
		URL url = Loader.getResource("src/main/resources/files/log4j.properties");
		PropertyConfigurator.configure(url);
	}

	public Object run(String fecha_ejecucion) {
			
		try {		
			logger.info("Realizando la consulta a la base de datos");
			Object result = this.getVector(queryextraccion,fecha_ejecucion);				
			return result;
		} catch (Exception e) {
			logger.error("Error Consultando base de datos.", e);
		}
		return null;
		
	}
	
	@Override
	public Object getVector(String query,String fecha){
		try	{		
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date fecha2= formatter.parse(fecha);
			java.sql.Date DateSql= new java.sql.Date(fecha2.getTime());
			Connection conexion = ConnectionProvider.getConnection();
			PreparedStatement pstmt = conexion.prepareStatement(query);
			
			pstmt.setObject(1, DateSql);
			pstmt.setObject(2, DateSql);
			return pstmt.executeQuery();				
					
		} catch (Exception e) {
			logger.error("Error SQL: ", e);
		}
		return null;
	}
	
}
