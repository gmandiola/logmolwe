package com.CsvToSftp.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public interface Services {
	 
	 public boolean extraccion(String output, String executionDate, Properties setting) throws IOException, SQLException;
	 
	 public boolean transferenciaSFTP(String output, String executionDate, Properties setting) throws IOException;

}
