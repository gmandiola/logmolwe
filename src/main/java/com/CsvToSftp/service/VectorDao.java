package com.CsvToSftp.service;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public interface VectorDao {

	public Object getVector(String query,String fecha) throws FileNotFoundException, SQLException;
}
