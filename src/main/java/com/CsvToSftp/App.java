package com.CsvToSftp;

import java.io.FileReader;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;
import com.CsvToSftp.service.Services;
import com.CsvToSftp.service.impl.ServicesImpl;

public class App 
{
	static final Logger logger = Logger.getLogger(App.class);
    public static void main( String[] args ) throws Exception
    {
     	try {
			Cli parsCli = new Cli(args);
			CommandLine parsEval = parsCli.parse();
			URL url = Loader.getResource("src/main/resources/files/log4j.properties");
			PropertyConfigurator.configure(url);
			
			Services amlService = new ServicesImpl();
			
			logger.info("fecha-ejecucion: " + parsEval.getOptionValue("f"));
			
			Properties settings = new Properties();
			settings.load(new FileReader("config/parameters.properties"));			
			
			String localInputFiles = "";
			
					if(!amlService.extraccion(localInputFiles, parsEval.getOptionValue("f"), settings))
						throw new RuntimeException("No fue completada la ejecución de la de extraccion y creacion CSV");
					
					if(!amlService.transferenciaSFTP(localInputFiles, parsEval.getOptionValue("f"), settings))
						throw new RuntimeException("No fue completada la ejecución de la tarea: transferenciaSFTP");
					
					
			logger.info("TAREA EJECUTADA SATISFACTORIAMENTE");			
			
		} catch (Exception e) {
			logger.error("LA TAREA NO FUE COMPLETADA", e);
			throw e;
		}	
}
}
