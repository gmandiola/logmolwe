package com.CsvToSftp.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.CsvToSftp.service.Services;
import com.CsvToSftp.sftp.SFTPConnector;

import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.csv.CSVFormat;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.ArrayUtils;
import com.jcraft.jsch.JSchException;



public class ServicesImpl implements Services{

	static final Logger logger = Logger.getLogger(ServicesImpl.class);
	
	public ServicesImpl() {
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean extraccion(String output, String executionDate, Properties setting) throws IOException, SQLException{
		
		VectorDaoJDBC dao = new VectorDaoJDBC();
		logger.info("Realizando la consulta a la base de datos");	
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date fecha= format.parse(executionDate);
			fecha.setHours(-24);
			executionDate=format.format(fecha);
		} catch (ParseException e1) {
			logger.error("Error formateando fecha", e1);
		}
		ResultSet rs5 = (ResultSet)dao.run(executionDate);
		 
		String stdFileName = "";
		try {
			stdFileName = this.getFileName(executionDate, setting);
		} catch (Exception e) {
			logger.error("Obteniendo nombre del archivo", e);
			return false;
		}
		try {
			crearCsv(stdFileName, rs5, output);
		} catch (Exception e) {
			logger.error("Error intentando crear archivo", e);
			return false;
		}
		finally{
			rs5.close();
		}
		return true;
	}
	
	@Override
	public boolean transferenciaSFTP(String output, String executionDate, Properties setting) throws IOException {
		
		logger.info("Iniciando proceso de transferencia SFTP.");
		String username = setting.getProperty("sftpuser");
		String privateKey = setting.getProperty("privatekeypath");
		String host = setting.getProperty("sftphost");
		Integer port = Integer.valueOf(setting.getProperty("sftpport"));
		String sftServerPath = setting.getProperty("sftserverpath");
		if(!sftServerPath.endsWith("/"))
  			 sftServerPath += "/";
	
		File[] files = new File(".").listFiles();
        List<String> filesFullPath = new ArrayList<>();
        List<String> filesName = new ArrayList<>();
        for(int i = 0; i < files.length; i++){
        	if(files[i].isFile() && files[i].getName().contains("LOGWE")){
        		filesFullPath.add(files[i].getName());
        		filesName.add(files[i].getName());
        	}
        }
		Object[] objectList = filesFullPath.toArray();        
        String[] pathFileNames = Arrays.copyOf(objectList, objectList.length, String[].class);
        
        objectList = filesName.toArray();  
        String[] fileNames = Arrays.copyOf(objectList, objectList.length, String[].class);
                
        boolean result = false;
        SFTPConnector sshConnector = new SFTPConnector();
        try {
        	logger.info("Solicitando conexión con el servidor.");
			sshConnector.connect(username, privateKey, host, port);
			
			logger.info("Se inicia transferencia de archivos.");
			sshConnector.addFile(sftServerPath, pathFileNames, fileNames);
			sshConnector.disconnect();
					result = true;
		} catch (IllegalAccessException e) {
			logger.error("Las credenciales de acceso al servidor no son válidas.", e);
		} catch (JSchException e) {
			logger.error("No se pudo establecer conexión con el servidor.", e);
		} catch (Exception e) {
			logger.error("Error durante la transferencia SFTP.", e);
		}
        finally {
        	for (int j = 0; j < fileNames.length; j++) {
				File archivo = new File(fileNames[j]);
			    if (archivo.exists()) {
			        archivo.delete();
			    }					
			}
        }
        return result;
		
	}
	
	private String getFileName(String executionDate, Properties setting) throws IOException{			
		String nameBase = setting.getProperty("basearchivo");
		String nameDate = executionDate.replace("-", "");	
		String year= nameDate.substring(0, 4).replace("20", "");
		String month= nameDate.substring(4,6);
		String day = nameDate.substring(6,8);
		String name = nameBase+day+month+year;
		return name;
		
	}
	
		@SuppressWarnings("unchecked")
	private void crearCsv(String rama, ResultSet rs, String output)throws SQLException, IOException{		
        ResultSetMetaData meta = rs.getMetaData(); 
        int numberOfColumns = meta.getColumnCount();       
   
        String[] dataHeadersArr = new String[numberOfColumns + 1];
        dataHeadersArr[0] = meta.getColumnName(1) ; 
        for (int i = 2 ; i < numberOfColumns + 1 ; i ++ ) {        	
        	if(!meta.getColumnName(i).trim().equals("") && meta.getColumnName(i) != null){        		
        			dataHeadersArr[i - 1] = meta.getColumnName(i);
        	}
        } 
        int deleteIndex = dataHeadersArr.length - 1;             
        int k = deleteIndex;
    	while(k >= 0 && (dataHeadersArr[k] == null || dataHeadersArr[k].trim().equals(""))){
    		dataHeadersArr = (String[]) ArrayUtils.remove(dataHeadersArr, k--);
    	}		
    	CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\r\n").withDelimiter(';').withQuote(null);
    	try(        		
	        FileWriter fileWriter = new FileWriter(output+rama  + ".txt");
    	    CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);     		
        ){
		try {
	        while (rs.next()) {
	        	@SuppressWarnings("rawtypes")
				List itemDataRecord = new ArrayList();
	        			 	        
	        	String row = rs.getString(1);
	        	itemDataRecord.add(row);
	        	for (int i = 2 ; i <= numberOfColumns; i++) {
	        		
	            	if(rs.getString(dataHeadersArr[i - 1]) != null){
	            		row = rs.getString(i);
	            	}
	            	else{
	            		row = " ";
	            	}		            	         
	            	itemDataRecord.add(row);		        		
	            }
	        	csvFilePrinter.printRecord(itemDataRecord);		        	
	        }
	        logger.info("Archivo Creado satisfactoriamente!");				
	        } catch (Exception e) {
	        	logger.error("Error creando archivo!", e);
			}
        }
	}
}
