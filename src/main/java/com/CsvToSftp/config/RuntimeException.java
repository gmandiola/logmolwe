package com.CsvToSftp.config;

@SuppressWarnings("serial")
public class RuntimeException extends Exception{

	public RuntimeException(String msg) {
        super(msg);
	}
}
